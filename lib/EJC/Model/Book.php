<?php

namespace EJC\Model;

/**
 * Model fuer den Customer
 *
 * @author Christian Hansen <christian.hansen@stud.fh-luebeck.de>
 * @package wp-crm
 */
class Book extends AbstractModel { 
    
    /**
     * Der Titel
     * 
     * @var string 
     */
    protected $title;
    
    /**
     * Der Autor
     *
     * @var string 
     */
    protected $author;

    /**
     * Der Kommentar
     *
     * @var string 
     */
    protected $comment;
    
    /**
     * Die Bewertung
     * 
     * @var int
     */
    protected $rating;
    
    /**
     * Das Themengebiet
     * 
     * @var int
     */
    protected $topic;
    
    public function __construct() {
        parent::__construct();
    }

    /**
     * Hole den Titel
     * 
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Setze den Titel
     * 
     * @param string $title
     * @return void
     */
    public function setTitle($title) {
        $this->title = trim($title);
    }
    
    /**
     * Hole den Autor
     * 
     * @return string
     */
    public function getAuthor() {
        return $this->author;
    }

    /**
     * Setze den Autor
     * 
     * @param string $author
     * @return void
     */
    public function setAuthor($author) {
        $this->author = trim($author);
    }
    
    /**
     * Hole den Kommentar
     * 
     * @return string
     */
    public function getComment() {
        return $this->comment;
    }     
    
    /**
     * Setze den Kommentar
     * 
     * @param string $comment
     * @return void
     */
    public function setComment($comment) {
        $this->comment = trim($comment);
    }
    
    /**
     * Hole das Themengebiet
     * 
     * @return array
     */
    public function getTopic() {
        if ($this->topic < 1) {
            return 1;
        }
        return (int) $this->topic;
    }

    /**
     * Hole den Namen des Themengebiets
     * 
     * @return array
     */
    public function getTopicName() {
        $topicRepository = new \EJC\Repository\TopicRepository();
        $topic = $topicRepository->findById($this->getTopic());
        if ($topic === NULL) {
            return 'nicht angegeben';
        }
        return $topic->getName();
    }
    
    /**
     * Setze das Themengebiet
     * 
     * @param int $topic
     * @return array
     */
    public function setTopic($topic) {
        $this->topic = (int) $topic;
    }
    
    /**
     * Hole alle möglichen Themengebiete
     * 
     * @return array
     */
    public function getPossibleTopics() {
        $topicRepository = new \EJC\Repository\TopicRepository();
        return $topicRepository->findAll('name ASC');
    }
    
    /**
     * Hole das Rating
     * 
     * @return int
     */
    public function getRating() {
        return (int) $this->rating;
    }
    
    /**
     * Setze das Rating
     * 
     * @param int $rating
     * @return void
     */
    public function setRating($rating) {
        $this->rating = (int) $rating;
    }
    
    /**
     * Hole den HTMl-Code für die Rating-Sternchen
     */
    public function getRatingStars() {
        $stars = '';
        for ($i = 1; $i <= 5; $i++) {
            if ($i <= $this->rating) {
                $stars .= '<span class="glyphicon glyphicon-star"></span>';
            } else {
                $stars .= '<span class="glyphicon glyphicon-star-empty"></span>';
            }
            $stars .= '&nbsp;';
        }
        return $stars;
    }
    
    /**
     * Hole den HTMl-Code für die Rating-Sternchen
     */
    public function getRatingStarsEdit() {
        $stars = '';
        for ($i = 1; $i <= 5; $i++) {
            if ($i <= $this->rating) {
                $stars .= '<span class="glyphicon glyphicon-star"></span>';
            } else {
                $stars .= '<span class="glyphicon glyphicon-star-empty"></span>';
            }
            $stars .= '&nbsp;';
        }
        return $stars;
    }    

}

