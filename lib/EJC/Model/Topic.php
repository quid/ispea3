<?php

namespace EJC\Model;

/**
 * Model fuer das Themengebiet
 *
 * @author Christian Hansen <christian.hansen@stud.fh-luebeck.de>
 * @package wp-crm
 */
class Topic extends AbstractModel { 

    /**
     * Die Strasse
     * 
     * @var string 
     */
    protected $name;
    
    /**
     * Hole den Namen
     * 
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Setze die Strasse
     * 
     * @param string $name
     * @return void
     */
    public function setName($name) {
        $this->name = trim($name);
    }
}

