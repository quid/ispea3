<?php

namespace EJC\Repository;

/**
 * Repository fuer die Buecher
 *
 * @author Chrstian Hansen <christian.hansen@stud.fh-luebeck.de>
 * @package wp-crm
 */
class BookRepository extends AbstractRepository {
    
    /**
	 * Konstruktor
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
    	// Setze die Tabelle
		$this->table = 'book';
	}

}

