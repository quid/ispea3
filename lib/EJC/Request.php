<?php

namespace EJC;

/**
 * Anhand der uebergebenen Parameter wird entschieden, welche Action aufgerufen
 * wird und was als Parameter an diese Action uebergeben wird
 *
 * @author Chrstian Hansen <christian.hansen@stud.fh-luebeck.de>
 * @package wp-crm
 */
class Request {

    protected $controller;
    protected $action;
    protected $view;
	protected $ajax;
    protected $params = array();
    protected $actionParams = array();

    /**
     * Konstruktor
     *
     * @return void
     */
    public function __construct($action = NULL, $controller = NULL, array $params = array(), View $view = NULL) {

        if ($controller !== NULL && $action !== NULL) {
            // Request wird ueber forward-Methode aus dem AbstractController aufgerufen
            $this->controller = $controller;
            $this->action = $action;
            $this->view = $view;
            foreach ($params AS $param) {
                if (is_object($param)) {
                    $this->actionParams[] = $param;
                } else {
                    $this->params[] = $param;
                }
            }
        } else {
            // HTTP-Request, der Request wird von der index.php instanziiert
            $getParams = $this->getGetParams();
            $postParams = $this->getPostParams();

            if (isset($getParams['controller'])) {
                $this->controller = ucwords(\EJC\Helper\StringHelper::cleanUp($getParams['controller']));
                unset($getParams['controller']);
            }
            if (isset($getParams['action'])) {
                $this->action = \EJC\Helper\StringHelper::cleanUp($getParams['action']);
                unset($getParams['action']);
            }            
			
            // Fuege GET- und POST-Paramenter zusammen
            $this->params = array_merge($getParams, $postParams);

            // Hole den Typen des uebergebenen Objekts
            foreach ($this->params AS $paramName => $paramValues) {

                // Erstelle ein neues Objekt des Typs, wenn der Anfang des
                // Parameternamens "new" ist
                if (substr($paramName, 0, 3) === 'new') {

                    // Schaue ob ein Model exisitert, wenn ja, dann erstelle neues Objekt
                    $newObjectClassName = '\\EJC\\Model\\' . ucWords(substr($paramName, 3));
                    $classFile = APPROOT . '/lib/' . str_replace('\\', '/', $newObjectClassName) . '.php';
                    if (file_exists($classFile)) {
                        $object = new $newObjectClassName();
                        // Setze die Eigenschaften fuer das neue Objekt, welche in
                        // den Paramtern uebergeben werden
                        if (is_array($paramValues)) {
                            foreach ($paramValues AS $paramValueKey => $paramValueValue) {
                                call_user_func_array(array($object, 'set' . ucwords($paramValueKey)), array($paramValueValue));
                                unset($paramValues[$paramValueKey]);
                            }
                        }
                        $this->actionParams = array_merge(array($object), $this->actionParams);
                        unset($this->params[$paramName]);
                    }
                } else {
                    // Pruefe ob ein Gegenpart zu dem Request im Repository existiert
                    // und lade diesen
                    $repositoryClassName = '\\EJC\\Repository\\' . ucWords($paramName) . 'Repository';
                    try {
                        // Erstelle ein Objekt der Repository-Klasse
                        $repository = new $repositoryClassName();
                        if (is_array($paramValues)) {
                            
                            // Finde das entsprechende Objekt in der DB
                            $object = $repository->findById((int) $paramValues['id']);
                            unset($paramValues['id']);

                            if ($object === NULL) {
                                throw new \EJC\Exception\RepositoryException('object has no counterpart in repository', 1366378567);
                            } else {
                                if (is_array($paramValues)) {

                                    // Wenn mehrere Paramter uebergeben werden, setze bei
                                    // dem geladenen Objekt die uebergebenen Eigenschaften
                                    foreach ($paramValues AS $paramValueKey => $paramValueValue) {
                                        call_user_func_array(array($object, 'set' . ucwords($paramValueKey)), array($paramValueValue));
                                        unset($paramValues[$paramValueKey]);
                                    }
                                }
                            }
                        }
                        $this->actionParams = array_merge(array($object), $this->actionParams);
                        unset($this->params[$paramName]);
                    } catch (\EJC\Exception\ClassLoaderException $e) {
                        // Es existiert kein Model zu dem Parameter
                        // tue weiter nichts und uebergebe den Parameter
                        //throw $e;
                    }
                }
                // Übergebe weitere Parameter
                $this->actionParams[$paramName] = $paramValues; 
            }
        }

        // Rufe default action auf, wenn controller und action nicht gesetzt
        if (empty($this->controller) && empty($this->action)) {
            $this->controller = 'Index';
            $this->action = 'index';
        }
    }

    /**
     * Rufe die action auf
     *
     * @return void
     */
    public function execute() {
        // Defieniere die aufzurufende Klasse und Action-Methode
        $controllerClassName = '\\EJC\\Controller\\' . $this->controller . 'Controller';
        $controller = new $controllerClassName($this, $this->view);
        $actionName = $this->action . 'Action';

        // Rufe die Action mit den Parametern auf
        call_user_func_array(array($controller, $actionName), $this->actionParams);
    }

    /**
     * Hole die Post-Parameter
     *
     * @return array
     */
    public function getPostParams() {
        $postParams = filter_input_array(INPUT_POST);
        return is_array($postParams) ? $postParams : array();
    }

    /**
     * Hole die Get-Parameter
     *
     * @return array
     */
    public function getGetParams() {
        $getParams = filter_input_array(INPUT_GET);
        return is_array($getParams) ? $getParams : array();
    }

    /**
     * Gib den Controller_Namen zum Request zurueck
     *
     * @return string
     */
    public function getController() {
        return $this->controller;
    }

    /**
     * Gib den Acton-Namen zum Request zurueck
     *
     * @return string
     */
    public function getAction() {
        return $this->action;
    }

    /**
     * Gib alle Paramter in einem Array zurueck
     *
     * @return array
     */
    public function getParams() {
        return $this->params;
    }

    /**
     * Hole die URL der aktuellen Seite
     *
     * @return string
     */
    public function getCurrentUrl() {
        return filter_input(INPUT_SERVER, 'REQUEST_URI');
    }

}