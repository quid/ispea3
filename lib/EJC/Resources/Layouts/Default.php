<!DOCTYPE html>
<html lang="de">
    <?php
    /**
     * Default Layout fuer http-Aufrufe
     *
     * @author Christian Hansen <christian.hansen@stud.fh-luebeck.de>
     */
    ?>
    <head>
        <title><?php echo $this->title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="author" content="Christian Hansen" />
        <meta name="robots" content="noindex, nofollow" />
        <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap-theme.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="css/screen.css" media="all" />
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <a class="navbar-brand" href="/">Literaturverwaltung</a>
                <ul class="nav navbar-nav">
                    <li><?php $this->getLink('<span class="glyphicon glyphicon-plus"></span> Neues Buch', 'Book', 'new'); ?></li>
                    <li><?php $this->getLink('Themengebiete', 'Topic', 'list'); ?></li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <h1><?php echo $this->title; ?></h1>
            <div id="main-content">
                <?php if (!empty($this->errors)): ?>
                    <div class="errors">
                        <?php echo $this->errors; ?>
                    </div>
                <?php endif; ?>
                <?php echo $this->template; ?>
            </div>
            <div id="footer">
            </div>
        </div>
        <script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>        
        <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    </body>
</html>