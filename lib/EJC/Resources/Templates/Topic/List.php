<table class="table table-striped">
    <tr>
        <th>
            Name&nbsp;
            <?php $this->getLink('<span class="glyphicon glyphicon-sort-by-alphabet"></span>', 'Topic', 'list', array('orderBy' => 'name ASC')); ?>
            <?php $this->getLink('<span class="glyphicon glyphicon-sort-by-alphabet-alt"></span>', 'Topic', 'list', array('orderBy' => 'name DESC')); ?>
        </th>
        <th></th>
    </tr>
<?php
foreach ($this->topics AS $topic):
    if ($topic->getId() > 1) {
        echo '<tr>';
        echo '<td>' . $topic->getName() . '</td>';
        echo '<td class="text-right">'; 
        $this->getLink('<span class="glyphicon glyphicon-remove"></span> löschen', 'Topic', 'delete', array('topic[id]' => $topic->getId())); 
        echo '</td>';
        echo '</tr>';
        }
endforeach;
?>
</table>

<h2>Neues Themengebiet anlegen</h2>
<form role="form" method="post" action="<?php echo $this->getUrl('Topic', 'create'); ?>">
    <div class="form-group">
        <label for="newtopic-title">Name</label>
        <input type="text" name="newTopic[name]" class="form-control" id="newtopic-title" placeholder="Name">
    </div>

    <button type="submit" class="btn btn-default">Speichern</button>
</form>