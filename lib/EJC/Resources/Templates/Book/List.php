<table class="table table-striped">
    <tr>
        <th>
            Titel&nbsp;
            <?php $this->getLink('<span class="glyphicon glyphicon-sort-by-alphabet"></span>', 'Book', 'list', array('orderBy' => 'title ASC')); ?>
            <?php $this->getLink('<span class="glyphicon glyphicon-sort-by-alphabet-alt"></span>', 'Book', 'list', array('orderBy' => 'title DESC')); ?>
        </th>
        <th>
            Autor&nbsp;
            <?php $this->getLink('<span class="glyphicon glyphicon-sort-by-alphabet"></span>', 'Book', 'list', array('orderBy' => 'author ASC')); ?>
            <?php $this->getLink('<span class="glyphicon glyphicon-sort-by-alphabet-alt"></span>', 'Book', 'list', array('orderBy' => 'author DESC')); ?>
        </th>
        <th>Themengebiet</th>
        <th>Bewertung&nbsp;
            <?php $this->getLink('<span class="glyphicon glyphicon-sort-by-attributes-alt"></span>', 'Book', 'list', array('orderBy' => 'rating DESC')); ?>
            <?php $this->getLink('<span class="glyphicon glyphicon-sort-by-attributes"></span>', 'Book', 'list', array('orderBy' => 'rating ASC')); ?>
        </th>
        <th></th>
    </tr>
<?php
foreach ($this->books AS $book):
    echo '<tr><td>';
    $this->getLink($book->getTitle(), 'Book', 'show', array('book[id]' => $book->getId()));
    echo '</td><td>';
    echo $book->getAuthor();
    echo '</td><td>';
    echo $book->getTopicName();
    echo '</td><td>';
    echo $book->getRatingStars();
    echo '</td><td class="text-right">'; 
    $this->getLink('<span class="glyphicon glyphicon-remove"></span> löschen', 'Book', 'delete', array('book[id]' => $book->getId())); 
    echo '</td></tr>';
endforeach;
?>
</table>
