<div class="container">
    <div class="row">
        <div class="col-lg-2 col-md-2 col-xs-2 col-sm-2">
            <p>Autor:</p>
        </div>
        <div class="col-lg-10 col-md-10 col-xs-10 col-sm-10">
            <p><?php echo $this->book->getAuthor(); ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-2 col-xs-2 col-sm-2">
            <p>Kommentar:</p>
        </div>
        <div class="col-lg-10 col-md-10 col-xs-10 col-sm-10">
            <p><?php echo $this->book->getComment(); ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-2 col-xs-2 col-sm-2">
            <p>Themengebiet:</p>
        </div>
        <div class="col-lg-10 col-md-10 col-xs-10 col-sm-10">
            <p><?php echo $this->book->getTopicName(); ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-2 col-xs-2 col-sm-2">
            <p>Bewertung:</p>
        </div>
        <div class="col-lg-10 col-md-10 col-xs-10 col-sm-10">
            <p><?php echo $this->book->getRatingStars(); ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-2 col-xs-2 col-sm-2">
            <p><?php $this->getLink('bearbeiten', 'Book', 'edit', array('book[id]' => $this->book->getId()), '.btn btn-default'); ?></p>
        </div>
        <div class="col-lg-10 col-md-10 col-xs-10 col-sm-10">
            
        </div>
    </div>
</div>