<form role="form" method="post" action="<?php echo $this->getUrl('Book', 'create'); ?>">
    <div class="form-group">
        <label for="newbook-title">Titel</label>
        <input type="text" name="newBook[title]" class="form-control" id="newbook-title" placeholder="Buchtitel">
    </div>
    <div class="form-group">
        <label for="author-title">Autor</label>
        <input type="text" name="newBook[author]" class="form-control" id="newbook-author" placeholder="Autor">
    </div>
    <div class="form-group">
        <label for="boot-topic">Themengebiet</label>
        <select name="newBook[topic]" class="form-control">
            <option value="0">nicht angegeben</option>
            <?php
                foreach ($this->topics AS $topic) {
                    echo '<option value="' . $topic->getId() . '">' . $topic->getName() . '</option>';
                }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="newbook-topic">Bewertung</label>
        <select id="newbook-topic" name="newBook[rating]" class="form-control">
            <option value="0"></option>
            <option value="1">1 Stern</option>
            <option value="2">2 Sterne</option>
            <option value="3">3 Sterne</option>
            <option value="4">4 Sterne</option>
            <option value="5">5 Sterne</option>
        </select>
    </div>    
    <div class="form-group">
        <label for="newbook-comment">Kommentar</label>
        <textarea id="newbook-comment" class="form-control" name="newBook[comment]" rows="3"></textarea>
    </div>    
    <button type="submit" class="btn btn-default">Speichern</button>
</form>


