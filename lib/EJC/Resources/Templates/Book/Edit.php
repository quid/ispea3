<form role="form" method="post" action="<?php echo $this->getUrl('Book', 'update'); ?>">
    <input type="hidden" name="book[id]" value="<?php echo $this->book->getId(); ?>">
    <div class="form-group">
        <label for="book-title">Titel</label>
        <input type="text" name="book[title]" class="form-control" value="<?php echo $this->book->getTitle(); ?>" id="book-title" placeholder="Buchtitel">
    </div>
    <div class="form-group">
        <label for="author-title">Autor</label>
        <input type="text" name="book[author]" class="form-control"  value="<?php echo $this->book->getAuthor(); ?>" id="book-author" placeholder="Autor">
    </div>
    <div class="form-group">
        <label for="book-topic">Themengebiet</label>
        <select id="book-topic" name="book[topic]" class="form-control">
            <option value="0">nicht angegeben</option>
            <?php
                foreach ($this->book->getPossibleTopics() AS $topic) {
                    echo '<option ' . ($this->book->getTopic() === $topic->getId() ? 'selected' : '') . ' value="' . $topic->getId() . '">' . $topic->getName() . '</option>';
                }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="book-topic">Bewertung</label>
        <select id="book-topic" name="book[rating]" class="form-control">
            <option value="0"></option>
            <option value="1" <?php $this->book->getRating() === 1 ? 'selected' : ''; ?>>1 Stern</option>
            <option value="2" <?php $this->book->getRating() === 2 ? 'selected' : ''; ?>>2 Sterne</option>
            <option value="3" <?php $this->book->getRating() === 3 ? 'selected' : ''; ?>>3 Sterne</option>
            <option value="4" <?php $this->book->getRating() === 4 ? 'selected' : ''; ?>>4 Sterne</option>
            <option value="5" <?php $this->book->getRating() === 5 ? 'selected' : ''; ?>>5 Sterne</option>
        </select>
    </div>      
    <div class="form-group">
        <label for="book-comment">Kommentar</label>
        <textarea id="book-comment" class="form-control" name="book[comment]" rows="3"><?php echo $this->book->getComment(); ?></textarea>
    </div>
    <button type="submit" class="btn btn-default">Speichern</button>
</form>


