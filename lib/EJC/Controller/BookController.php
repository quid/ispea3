<?php

namespace EJC\Controller;

/**
 * Controller fuer den User
 *
 * @author Christian Hansen <christian.hansen@stud.fh-luebeck.de>
 * @package wp-crm
 */
class BookController extends AbstractController {

    /**
     * @var \EJC\Repository\BookRepository
     */
    protected $bookRepository;

    public function __construct(\EJC\Request $request, \EJC\View $view = NULL) {
        parent::__construct($request, $view);
        $this->bookRepository = new \EJC\Repository\BookRepository();
    }

    /**
     * Liste alle Bücher auf
     * 
     * @return void
     */
    public function listAction($orderBy = 'title ASC') {
        $this->view->assign('title', 'Übersicht');
        $this->view->assign('books', $this->bookRepository->findAll($orderBy));
        $this->view->render();
    }
    
    public function showAction(\EJC\Model\Book $book) {
        $this->view->assign('title', $book->getTitle());
        $this->view->assign('book', $book);
        $this->view->render();
    }
    
    public function newAction() {
        $topicRepository = new \EJC\Repository\TopicRepository();
        $this->view->assign('title', 'Neues Buch anlegen');
        $this->view->assign('topics', $topicRepository->findAll('name ASC'));
        $this->view->render();
    }
    
    public function editAction(\EJC\Model\Book $book) {
        $this->view->assign('title', $book->getTitle());
        $this->view->assign('book', $book);
        $this->view->render();
    }
    
    public function createAction(\EJC\Model\Book $newBook) {
        $insertId = $this->bookRepository->add($newBook);
        $this->redirect('Book', 'show', array('book[id]' => $insertId));
    }
    
    public function updateAction(\EJC\Model\Book $book) {
        $this->bookRepository->update($book);
        $this->redirect('Book', 'show', array('book[id]' => $book->getId()));
    }
    
    public function deleteAction(\EJC\Model\Book $book) {
        $this->bookRepository->remove($book);
        $this->redirect('Book', 'list');
    }
        
}
