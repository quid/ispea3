<?php

namespace EJC\Controller;

/**
 * Controller fuer den User
 *
 * @author Christian Hansen <christian.hansen@stud.fh-luebeck.de>
 * @package wp-crm
 */
class TopicController extends AbstractController {

    /**
     * @var \EJC\Repository\BookRepository
     */
    protected $topicRepository;

    public function __construct(\EJC\Request $request, \EJC\View $view = NULL) {
        parent::__construct($request, $view);
        $this->topicRepository = new \EJC\Repository\TopicRepository();
    }

    /**
     * Liste alle Themengebiete auf
     * 
     * @return void
     */
    public function listAction($orderBy = 'name ASC') {
        $this->view->assign('title', 'Themengebiete');
        $this->view->assign('topics', $this->topicRepository->findAll($orderBy));
        $this->view->render();
    }
    
    public function newAction() {
        $this->view->assign('title', 'Neues Themengebiet anlegen');
        $this->view->render();
    }
    
    public function editAction(\EJC\Model\Topic $topic) {
        $this->view->assign('title', $topic->getName() . ' bearbeiten');
        $this->view->render();
    }
    
    public function createAction(\EJC\Model\Topic $newTopic) {
        $this->topicRepository->add($newTopic);
        $this->redirect('Topic', 'list');
    }
    
    public function updateAction(\EJC\Model\Topic $topic) {
        $this->topicRepository->update($topic);
        $this->redirect('Topic', 'list');
    }
    
    public function deleteAction(\EJC\Model\Topic $topic) {
        $this->topicRepository->remove($topic);
        $this->redirect('Topic', 'list');
    }
        
}
