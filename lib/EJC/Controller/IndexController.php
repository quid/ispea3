<?php

namespace EJC\Controller;

/**
 * Controller fuer den User
 *
 * @author Christian Hansen <christian.hansen@stud.fh-luebeck.de>
 * @package wp-crm
 */
class IndexController extends AbstractController {

    /**
     * 
     * 
     * @return void
     */
    public function indexAction() {
        $this->forward('Book', 'list');
    }
    
  
}
